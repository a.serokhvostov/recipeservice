FROM maven:3-jdk-11 AS build
WORKDIR /usr/src/app
COPY . .
RUN mvn clean package -DskipTests

FROM adoptopenjdk:11-jre-hotspot
ARG JAR_FILE=/usr/src/app/target/*.jar
COPY --from=build ${JAR_FILE} application.jar
ENTRYPOINT ["java", "-jar", "application.jar"]