CREATE TABLE IF NOT EXISTS image_table
(
    "id"         serial NOT NULL,
    name       text NOT NULL UNIQUE,
    pic_byte bytea,
    type       text NOT NULL,
    CONSTRAINT image_table_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS recipe
(
    "id"         serial NOT NULL,
    name       text NOT NULL,
    cooking_time bigint NOT NULL,
    ingredients text NOT NULL,
    image_id bigint,
    description text NOT NULL,
    ranking double precision NOT NULL,
    votes bigint NOT NULL,
    CONSTRAINT recipe_pkey PRIMARY KEY (id),
    CONSTRAINT ranking_boundaries CHECK (ranking >= 0 AND ranking <= 5),
    CONSTRAINT FK_211 FOREIGN KEY ( image_id ) REFERENCES image_table ( "id" ) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS offered_recipe
(
    "id"         serial NOT NULL,
    name       text NOT NULL,
    cooking_time bigint NOT NULL,
    ingredients text NOT NULL,
    image_id bigint,
    description text NOT NULL,
    CONSTRAINT offered_recipe_pkey PRIMARY KEY (id),
    CONSTRAINT FK_214 FOREIGN KEY ( image_id ) REFERENCES image_table ( "id" ) ON DELETE CASCADE
);