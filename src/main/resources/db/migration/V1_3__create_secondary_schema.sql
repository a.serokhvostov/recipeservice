CREATE TABLE IF NOT EXISTS recipe_comments
(
    "id"         serial NOT NULL,
    message       text NOT NULL,
    publication_time time NOT NULL,
    user_id bigint NOT NULL,
    recipe_id bigint NOT NULL,
    CONSTRAINT recipe_comments_pkey PRIMARY KEY (id),
    CONSTRAINT FK_212 FOREIGN KEY ( user_id ) REFERENCES t_user ( user_id ) ON DELETE CASCADE,
    CONSTRAINT FK_213 FOREIGN KEY ( recipe_id ) REFERENCES recipe ( "id" ) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS user_favorite
(
    user_id bigint NOT NULL,
    recipe_id bigint NOT NULL,
    CONSTRAINT user_favorite_pkey PRIMARY KEY (user_id, recipe_id),
    CONSTRAINT FK_217 FOREIGN KEY ( user_id ) REFERENCES t_user ( user_id ) ON DELETE CASCADE,
    CONSTRAINT FK_218 FOREIGN KEY ( recipe_id ) REFERENCES recipe ( "id" ) ON DELETE CASCADE
);