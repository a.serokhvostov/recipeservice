CREATE TABLE IF NOT EXISTS t_user
(
 user_id               serial NOT NULL,
 username               text NOT NULL UNIQUE,
 password               text NOT NULL,
 CONSTRAINT t_user_pk PRIMARY KEY ( user_id )
);

CREATE TABLE IF NOT EXISTS roles (
  role_id serial NOT NULL,
  name text NOT NULL UNIQUE,
  CONSTRAINT roles_pk PRIMARY KEY (role_id)
);

CREATE TABLE IF NOT EXISTS users_roles (
  user_id bigint NOT NULL,
  role_id bigint NOT NULL,
  CONSTRAINT PK_users_roles PRIMARY KEY ( user_id, role_id ),
  CONSTRAINT role_fk FOREIGN KEY (role_id) REFERENCES roles (role_id) ON DELETE CASCADE,
  CONSTRAINT user_fk FOREIGN KEY (user_id) REFERENCES t_user (user_id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS user_favorite
(
 user_id  integer NOT NULL,
 recipe_id integer NOT NULL,
 CONSTRAINT PK_user_favorite PRIMARY KEY ( user_id, recipe_id ),
 CONSTRAINT FK_276 FOREIGN KEY ( user_id ) REFERENCES t_user ( user_id) ON DELETE CASCADE,
 CONSTRAINT FK_279 FOREIGN KEY ( recipe_id ) REFERENCES recipe ( "id" ) ON DELETE CASCADE
);

CREATE INDEX IF NOT EXISTS fkIdx_277 ON user_favorite
(
 user_id
);

CREATE INDEX IF NOT EXISTS fkIdx_280 ON user_favorite
(
 recipe_id
);