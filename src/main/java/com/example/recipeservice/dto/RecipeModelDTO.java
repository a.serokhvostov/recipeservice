package com.example.recipeservice.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Сущность рецепта")
public class RecipeModelDTO {
    @Schema(description = "Название рецепта")
    private String name;
    @Schema(description = "Время готовки в минутах (целое)")
    private long cookingTime;
    @Schema(description = "Ингредиенты (через запятую)", example = "Макароны, помидоры, огурцы")
    private String ingredients;
    @Schema(description = "Описание (в свободной форме)")
    private String description;
    @Schema(description = "Идентификатор изображения из базы")
    Long imageId;
}
