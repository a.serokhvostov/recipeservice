package com.example.recipeservice.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info(
                        new Info()
                                .title("Recipe Service Api")
                                .version("1.0.0")
                                .contact(
                                        new Contact()
                                                .email("a.serokhvostov@g.nsu.ru")
                                                .url("https://gitlab.com/a.serokhvostov/recipeservice")
                                                .name("Serokhvostov Alexandr")
                                )
                );
    }

}