package com.example.recipeservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.lang.Nullable;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "recipe")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class RecipeModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private long cookingTime;
    private String ingredients;
    private String description;
    private float ranking;
    private long votes;

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "image_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    ImageModel imageId;

    //Constructors, getters, setters

    public RecipeModel() {
        name = "";
        imageId = new ImageModel();
    }

    public RecipeModel(long id, String name, long cookingTime, String ingredients, String description) {
        this.id = id;
        this.name = name;
        this.cookingTime = cookingTime;
        this.ingredients = ingredients;
        this.description = description;
        this.imageId = new ImageModel();
    }
}

