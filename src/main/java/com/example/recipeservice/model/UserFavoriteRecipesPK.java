package com.example.recipeservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@Embeddable
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class UserFavoriteRecipesPK implements Serializable {
    @Column(name = "user_id")
    private long userId;
    @Column(name = "recipe_id")
    private long recipeId;

    public UserFavoriteRecipesPK() {
    }

    public UserFavoriteRecipesPK(long userId, long recipeId) {
        this.userId = userId;
        this.recipeId = recipeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserFavoriteRecipesPK neighborhoodId = (UserFavoriteRecipesPK) o;
        return userId == neighborhoodId.userId &&
                recipeId == neighborhoodId.recipeId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, recipeId);
    }
}