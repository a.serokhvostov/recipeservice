package com.example.recipeservice.service;

import com.example.recipeservice.model.ImageModel;
import com.example.recipeservice.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ImageService {
    @Autowired
    ImageRepository imageRepository;

    public Optional<ImageModel> findByName(String name) {
        return imageRepository.findByName(name);
    }

    public ImageModel save(ImageModel imageModel) {
        return imageRepository.save(imageModel);
    }
    public Optional<ImageModel> findById(Long imageId) {
        return imageRepository.findById(imageId);
    }
}
