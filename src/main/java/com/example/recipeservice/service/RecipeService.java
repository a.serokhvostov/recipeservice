package com.example.recipeservice.service;

import com.example.recipeservice.model.RecipeModel;
import com.example.recipeservice.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RecipeService {
    @Autowired
    RecipeRepository recipeRepository;

    public Page<RecipeModel> searchRecipesByIngredients(String recipeIngredients, Pageable pageable) {
        return recipeRepository.searchRecipesByIngredients(recipeIngredients, pageable);
    }
    public Page<RecipeModel> searchRecipesByName(String recipeName, Pageable pageable) {
        return recipeRepository.searchRecipesByName(recipeName, pageable);
    }
    public RecipeModel save(RecipeModel recipeModel) {
        return recipeRepository.save(recipeModel);
    }
    public void delete(RecipeModel recipeModel) {
        recipeRepository.delete(recipeModel);
    }
    public Optional<RecipeModel> findById(Long recipeId) {
        return recipeRepository.findById(recipeId);
    }
    public Page<RecipeModel> findAll(Pageable pageable) {
        return recipeRepository.findAll(pageable);
    }
}
