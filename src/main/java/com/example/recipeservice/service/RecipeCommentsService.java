package com.example.recipeservice.service;

import com.example.recipeservice.model.RecipeComments;
import com.example.recipeservice.model.RecipeModel;
import com.example.recipeservice.repository.RecipeCommentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RecipeCommentsService {
    @Autowired
    RecipeCommentsRepository recipeCommentsRepository;

    public Optional<RecipeComments> findById(Long recipeCommentsId) {
        return recipeCommentsRepository.findById(recipeCommentsId);
    }
    public Page<RecipeComments> findByRecipeId(RecipeModel recipeId, Pageable pageable) {
        return recipeCommentsRepository.findByRecipeId(recipeId, pageable);
    }
    public Page<RecipeComments> findAll(Pageable pageable) {
        return recipeCommentsRepository.findAll(pageable);
    }
    public RecipeComments save(RecipeComments recipeComments) {
        return recipeCommentsRepository.save(recipeComments);
    }
    public void delete(RecipeComments recipeComments) {
        recipeCommentsRepository.delete(recipeComments);
    }
}
