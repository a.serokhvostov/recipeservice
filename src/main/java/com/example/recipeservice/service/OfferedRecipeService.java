package com.example.recipeservice.service;

import com.example.recipeservice.model.OfferedRecipe;
import com.example.recipeservice.repository.OfferedRecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OfferedRecipeService {
    @Autowired
    OfferedRecipeRepository offeredRecipeRepository;

    public Optional<OfferedRecipe> findById(Long offeredRecipeId) {
        return offeredRecipeRepository.findById(offeredRecipeId);
    }
    public Page<OfferedRecipe> findAll(Pageable pageable) {
        return offeredRecipeRepository.findAll(pageable);
    }
    public OfferedRecipe save(OfferedRecipe offeredRecipe) {
        return offeredRecipeRepository.save(offeredRecipe);
    }
    public void delete(OfferedRecipe offeredRecipe) {
        offeredRecipeRepository.delete(offeredRecipe);
    }
}
