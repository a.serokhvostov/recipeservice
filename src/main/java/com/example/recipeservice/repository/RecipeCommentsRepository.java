package com.example.recipeservice.repository;

import com.example.recipeservice.model.RecipeComments;
import com.example.recipeservice.model.RecipeModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecipeCommentsRepository extends JpaRepository<RecipeComments, Long>{
    Page<RecipeComments> findByRecipeId(RecipeModel recipeId, Pageable pageable);
}