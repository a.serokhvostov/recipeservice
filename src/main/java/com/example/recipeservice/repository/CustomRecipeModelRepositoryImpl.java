package com.example.recipeservice.repository;

import com.example.recipeservice.model.RecipeModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomRecipeModelRepositoryImpl implements CustomRecipeModelRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<RecipeModel> searchRecipesByIngredients(String recipeIngredients, Pageable pageable) {
        var cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<RecipeModel> q = cb.createQuery(RecipeModel.class);
        Root<RecipeModel> recipe = q.from(RecipeModel.class);

        Path<String> ingredientsPath = recipe.get("ingredients");
        List<Predicate> predicates = new ArrayList<>();
        for(String word : recipeIngredients.split(",")) {
            Expression<String> wordLiteral = cb.literal("%" + word + "%");
            predicates.add(cb.like(cb.lower(ingredientsPath), cb.lower(wordLiteral)));
        }
        q.select(recipe).where(
                cb.and(predicates.toArray(new Predicate[predicates.size()]))
        );

        List<RecipeModel> hits = entityManager.createQuery(q).getResultList();
        int start = (int)pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), hits.size());
        return new PageImpl<>(hits.subList(start, end), pageable, hits.size());
    }

    @Override
    public Page<RecipeModel> searchRecipesByName(String recipeName, Pageable pageable) {
        var cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<RecipeModel> q = cb.createQuery(RecipeModel.class);
        Root<RecipeModel> recipe = q.from(RecipeModel.class);

        Path<String> namePath = recipe.get("name");
        List<Predicate> predicates = new ArrayList<>();
        for(String word : recipeName.split(" ")) {
            Expression<String> wordLiteral = cb.literal("%" + word + "%");
            predicates.add(cb.like(cb.lower(namePath), cb.lower(wordLiteral)));
        }
        q.select(recipe).where(
                cb.and(predicates.toArray(new Predicate[predicates.size()]))
        );

        List<RecipeModel> hits = entityManager.createQuery(q).getResultList();
        int start = (int)pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), hits.size());
        return new PageImpl<>(hits.subList(start, end), pageable, hits.size());
    }
}