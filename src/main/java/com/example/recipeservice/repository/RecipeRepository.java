package com.example.recipeservice.repository;

import com.example.recipeservice.model.RecipeModel;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;



public interface RecipeRepository extends JpaRepository<RecipeModel, Long>, CustomRecipeModelRepository {
    Page<RecipeModel> searchRecipesByIngredients(String recipeIngredients, Pageable pageable);
    Page<RecipeModel> searchRecipesByName(String recipeName, Pageable pageable);

}