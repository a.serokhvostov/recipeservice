package com.example.recipeservice.repository;


import java.util.Optional;

import com.example.recipeservice.model.ImageModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<ImageModel, Long> {
    Optional<ImageModel> findByName(String name);
}