package com.example.recipeservice.repository;

import com.example.recipeservice.model.UserFavoriteRecipes;
import com.example.recipeservice.model.UserFavoriteRecipesPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface UserFavoriteRecipesRepository extends JpaRepository<UserFavoriteRecipes, UserFavoriteRecipesPK> {
    Optional<UserFavoriteRecipes> findByIdUserIdAndIdRecipeId
            (Long userId, Long recipeId);
    Page<UserFavoriteRecipes> findByIdUserId(Long userId, Pageable pageable);

}
