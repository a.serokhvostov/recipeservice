package com.example.recipeservice.repository;

import com.example.recipeservice.model.OfferedRecipe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfferedRecipeRepository extends JpaRepository<OfferedRecipe, Long> {
}
