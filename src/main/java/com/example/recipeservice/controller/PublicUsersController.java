package com.example.recipeservice.controller;

import com.example.recipeservice.auth.UserAuthenticationService;
import com.example.recipeservice.exception.ResourceNotFoundException;
import com.example.recipeservice.model.User;
import com.example.recipeservice.repository.RoleRepository;
import com.example.recipeservice.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

@RestController
@RequestMapping("/api/public/users")
@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor(access = PACKAGE)
@Tag(name = "Контроллер пользователей", description = "Позволяет регистрироваться, входить в систему и находить пользователя по токену.")
final class PublicUsersController {
    @Autowired
    UserAuthenticationService authentication;
    @Autowired
    UserService users;

    @Autowired
    RoleRepository roleRepository;

    @Operation(summary="Зарегистрироваться.",
            description="Позволяет зарегистрироваться и получить роль пользователя. Доступ - все.")
    @PostMapping("/register")
    public String register(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password) {
        var user = User
                .builder()
                .username(username)
                .password(password)
                .build();
        if(roleRepository.findByName("ROLE_USER").isPresent()) {
            user.addRole(roleRepository.findByName("ROLE_USER").get());
        } else throw new ResourceNotFoundException("Role not found with name ROLE_USER.");
        users.save(user);

        return login(username, password);
    }
    @Operation(summary="Получить информацию о пользователе по токену.",
            description="Позволяет информацию о пользователе по токену. Доступ - все.")
    @PostMapping("/find_by_token")
    public User findByToken(@RequestParam("token") final String token)
    {
        var user = authentication.findByToken(token);
        if(user.isPresent())
            return user.get();
        else throw new ResourceNotFoundException("User not found with this token");

    }
    @Operation(summary="Войти.",
            description="Позволяет войти в систему. Доступ - все.")
    @PostMapping("/login")
    public String login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password) {
        return authentication
                .login(username, password)
                .orElseThrow(() -> new RuntimeException("invalid login and/or password"));
    }
}
