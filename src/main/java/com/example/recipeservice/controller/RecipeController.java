package com.example.recipeservice.controller;


import com.example.recipeservice.dto.RecipeModelDTO;
import com.example.recipeservice.exception.ResourceNotFoundException;
import com.example.recipeservice.model.RecipeModel;
import com.example.recipeservice.service.ImageService;
import com.example.recipeservice.service.RecipeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api")
@Tag(name = "Контроллер рецептов", description = "Позволяет отображать, производить поиск, загружать и удалять рецепты.")
public class RecipeController {
    private static final Logger log = LoggerFactory.getLogger(RecipeController.class);
    static final String RECIPE_NOT_FOUND_WITH_ID = "Recipe not found with id ";

    @Autowired
    private RecipeService recipeService;

    @Autowired
    private ImageService imageService;


    @Operation(summary="Отображение рецептов",
            description="Позволяет получить страницу с рецептами. Доступ - все.")
    @GetMapping("/public/recipe")
    public Page<RecipeModel> getRecipes(Pageable pageable) {
        log.info("Executing RecipeController.getRecipes.");
        return recipeService.findAll(pageable);
    }

    @Operation(summary="Рецепт по идентификатору",
            description="Позволяет получить рецепт по идентификатору. Доступ - все.")
    @GetMapping("/public/recipe/id/{recipeId}")
    public RecipeModel getRecipes(@PathVariable @Parameter(description = "Идентификатор рецепта.")
                                              Long recipeId) {
        log.info("Executing RecipeController.getRecipes.");
        var recipe = recipeService.findById(recipeId);
        if(recipe.isPresent())
            return recipe.get();
        else throw new ResourceNotFoundException(RECIPE_NOT_FOUND_WITH_ID + recipeId);
    }

    @Operation(summary="Поиск рецепта по имени",
            description="Позволяет получить страницу рецептов, соответствующую поиску по имени. Доступ - все.")
    @GetMapping("/public/recipe/{recipeName}")
    public Page<RecipeModel> getRecipesByName(@PathVariable @Parameter(description = "Название рецепта.")
                                                          String recipeName, Pageable pageable) {
        log.info("Executing RecipeController.getRecipesByName.");
        return recipeService.searchRecipesByName(recipeName, pageable);
    }

    @Operation(summary="Поиск рецепта по ингредиентам",
            description="Позволяет получить страницу рецептов, соответствующую поиску по ингредиентам. Доступ - все.")
    @GetMapping("/public/recipe/ingredients/{recipeIngredients}")
    public Page<RecipeModel> getRecipesByIngredients(@PathVariable @Parameter(description = "Список ингредиентов.")
                                                                 String recipeIngredients, Pageable pageable) {
        log.info("Executing RecipeController.getRecipesByIngredients.");
        return recipeService.searchRecipesByIngredients(recipeIngredients, pageable);
    }

    @Operation(summary="Сохранение рецепта",
            description="Позволяет сохранять новый рецепт. Доступ - администраторы.")
    @PostMapping("/create/recipe")
    public RecipeModel createRecipe(@Valid @RequestBody @Parameter(description = "Сущность рецепта.")
                                                RecipeModelDTO recipeRequest) {
        log.info("Executing RecipeController.createRecipe.");
        var recipe = new RecipeModel();
        recipe.setCookingTime(recipeRequest.getCookingTime());
        recipe.setDescription(recipeRequest.getDescription());
        recipe.setIngredients(recipeRequest.getIngredients());
        recipe.setName(recipeRequest.getName());
        recipe.setRanking(0);
        recipe.setVotes(0);
        if(recipeRequest.getImageId() == null) {
            recipe.setImageId(null);
            return recipeService.save(recipe);
        }
        else return imageService.findById(recipeRequest.getImageId())
                .map(image -> {
                    recipe.setImageId(image);
                    return recipeService.save(recipe);
                }).orElseThrow(() -> new ResourceNotFoundException("Image not found with id " + recipeRequest.getImageId()));
    }

    @Operation(summary="Обновление рецепта",
            description="Позволяет обновить рецепт из базы. Доступ - модераторы и администраторы.")
    @PutMapping("/edit/recipe/{recipeId}")
    public RecipeModel updateRecipe(@PathVariable @Parameter(description = "Идентификатор рецепта") Long recipeId,
                           @Valid @RequestBody @Parameter(description = "Сущность рецепта.") RecipeModelDTO recipeRequest) {
        log.info("Executing RecipeController.updateRecipe.");
        if(!imageService.findById(recipeRequest.getImageId()).isPresent()) {
            throw new ResourceNotFoundException("Image not found with id " + recipeRequest.getImageId());
        }
        return recipeService.findById(recipeId)
                .map(recipe -> {
                    recipe.setName(recipeRequest.getName());
                    recipe.setImageId(imageService.findById(recipeRequest.getImageId()).get());
                    recipe.setCookingTime(recipeRequest.getCookingTime());
                    recipe.setDescription(recipeRequest.getDescription());
                    recipe.setIngredients(recipeRequest.getIngredients());
                    return recipeService.save(recipe);
                }).orElseThrow(() -> new ResourceNotFoundException(RECIPE_NOT_FOUND_WITH_ID + recipeId));
    }

    @Operation(summary="Оценить рецепт",
            description="Позволяет оценить рецепт (оценка от 0 до 5). Доступ - авторизированные пользователи.")
    @PutMapping("/recipe/rate/{recipeId}")
    public RecipeModel rateRecipe(@PathVariable @Parameter(description = "Идентификатор рецепта") Long recipeId,
                                    @Valid @RequestBody @Parameter(description = "Оценка") Float ratingValue) {
        return recipeService.findById(recipeId)
                .map(recipe -> {
                    final long votes = recipe.getVotes() + 1;
                    recipe.setVotes(votes);
                    recipe.setRanking(recipe.getRanking()*(votes-1)/votes + ratingValue/votes);
                    return recipeService.save(recipe);
                }).orElseThrow(() -> new ResourceNotFoundException(RECIPE_NOT_FOUND_WITH_ID + recipeId));
    }

    @Operation(summary="Удаление рецепта",
            description="Позволяет удалить рецепт из базы. Доступ - администраторы.")
    @DeleteMapping("/delete/recipe/{recipeId}")
    public ResponseEntity<Object> deleteRecipe(@PathVariable @Parameter(description = "Идентификатор рецепта")
                                                           Long recipeId) {
        log.info("Executing RecipeController.deleteRecipe.");
        return recipeService.findById(recipeId)
                .map(recipe -> {
                    recipeService.delete(recipe);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException(RECIPE_NOT_FOUND_WITH_ID + recipeId));
    }
}
