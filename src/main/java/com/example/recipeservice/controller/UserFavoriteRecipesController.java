package com.example.recipeservice.controller;

import com.example.recipeservice.auth.UserAuthenticationService;
import com.example.recipeservice.dto.UserFavoriteRecipesDTO;
import com.example.recipeservice.exception.ResourceNotFoundException;
import com.example.recipeservice.model.User;
import com.example.recipeservice.model.UserFavoriteRecipes;
import com.example.recipeservice.model.UserFavoriteRecipesPK;
import com.example.recipeservice.service.RecipeService;
import com.example.recipeservice.service.UserFavoriteRecipesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@Tag(name = "Контроллер любимых рецептов пользователей",
        description = "Позволяет отображать, производить поиск, загружать и удалять любимые рецепты пользователей.")
@RestController
public class UserFavoriteRecipesController {
    private static final Logger log = LoggerFactory.getLogger(UserFavoriteRecipesController.class);
    private static final String RECIPE_NOT_FOUND_WITH_ID = "Recipe not found with id ";
    private static final String USER_NOT_FOUND_WITH_THIS_TOKEN = "User not found with this token";
    @Autowired
    private UserFavoriteRecipesService userFavoriteRecipesService;

    @Autowired
    private RecipeService recipeService;

    @Autowired
    private UserAuthenticationService userAuthenticationService;

    @Operation(summary="Получить все любимые рецепты пользователя.",
            description="Позволяет получить все любимые рецепты пользователя по токену. Доступ - авторизированные пользователи.")
    @GetMapping("/userFavoriteRecipes/{token}")
    public Page<UserFavoriteRecipes> getUserFavoriteRecipes(@PathVariable
                                                                @Parameter(description = "Токен") String token, Pageable pageable) {
        User user;
        if(userAuthenticationService.findByToken(token).isPresent()) {
            user = userAuthenticationService.findByToken(token).get();
        } else throw new ResourceNotFoundException(USER_NOT_FOUND_WITH_THIS_TOKEN);

        return userFavoriteRecipesService.findByIdUserId(user.getId(), pageable);
    }

    @Operation(summary="Получить любимый рецепт пользователя.",
            description="Позволяет получить любимый рецепт пользователя по идентификатору и токену. Доступ - авторизированные пользователи.")
    @GetMapping("/userFavoriteRecipes/{recipeId}/{token}")
    public UserFavoriteRecipes getUserFavoriteRecipesByIds(@PathVariable @Parameter(description = "Идентификатор") Long recipeId,
                                                                 @PathVariable @Parameter(description = "Токен") String token) {
        if(!recipeService.findById(recipeId).isPresent()) {
            throw new ResourceNotFoundException(RECIPE_NOT_FOUND_WITH_ID + recipeId);
        }
        User user;
        if(userAuthenticationService.findByToken(token).isPresent()) {
            user = userAuthenticationService.findByToken(token).get();
        } else throw new ResourceNotFoundException(USER_NOT_FOUND_WITH_THIS_TOKEN);

        if(userFavoriteRecipesService.findByIdUserIdAndIdRecipeId(user.getId(), recipeId).isPresent()) {
            return userFavoriteRecipesService.findByIdUserIdAndIdRecipeId(user.getId(), recipeId).get();
        } else throw new ResourceNotFoundException("UserFavoriteRecipe not found with this recipeId and token");


    }

    @Operation(summary="Добавить любимый рецепт пользователя.",
            description="Позволяет добавить любимый рецепт пользователя. Доступ - авторизированные пользователи.")
    @PostMapping("/userFavoriteRecipes")
    public UserFavoriteRecipes createUserFavoriteRecipes(@Valid @RequestBody UserFavoriteRecipesDTO
                                                                                       userFavoriteRecipesRequest) {
        var userFavoriteRecipes = new UserFavoriteRecipes();
        if(recipeService.findById(
                userFavoriteRecipesRequest.getRecipeId()).isPresent()) {
            userFavoriteRecipes.setRecipeId(recipeService.findById(
                    userFavoriteRecipesRequest.getRecipeId()).get());
        } else throw new ResourceNotFoundException(RECIPE_NOT_FOUND_WITH_ID + userFavoriteRecipesRequest.getRecipeId());
        User user;
        if(userAuthenticationService.findByToken(userFavoriteRecipesRequest.getToken()).isPresent()) {
            user = userAuthenticationService.findByToken(userFavoriteRecipesRequest.getToken()).get();
        } else throw new ResourceNotFoundException(USER_NOT_FOUND_WITH_THIS_TOKEN);

        userFavoriteRecipes.setUserId(user);
        var userFavoriteRecipesPK = new UserFavoriteRecipesPK();
        userFavoriteRecipesPK.setRecipeId(userFavoriteRecipesRequest.getRecipeId());
        userFavoriteRecipesPK.setUserId(user.getId());
        userFavoriteRecipes.setId(userFavoriteRecipesPK);

        return userFavoriteRecipesService.save(userFavoriteRecipes);
    }

    @Operation(summary="Удалить любимый рецепт пользователя.",
            description="Позволяет удалить любимый рецепт пользователя по идентификатору и токену. Доступ - авторизированные пользователи.")
    @DeleteMapping("/userFavoriteRecipes/{recipeId}/{token}")
    public ResponseEntity<Object> deleteUserFavoriteRecipes(@PathVariable @Parameter(description = "Идентификатор") Long recipeId,
                                                            @PathVariable @Parameter(description = "Токен") String token) {
        log.info("Deleting userFavoriteRecipes with recipeId={} and token={}", recipeId, token);
        if(!recipeService.findById(recipeId).isPresent()) {
            throw new ResourceNotFoundException(RECIPE_NOT_FOUND_WITH_ID + recipeId);
        }
        User user;
        if(userAuthenticationService.findByToken(token).isPresent()) {
            user = userAuthenticationService.findByToken(token).get();
        } else throw new ResourceNotFoundException(USER_NOT_FOUND_WITH_THIS_TOKEN);

        return userFavoriteRecipesService.findByIdUserIdAndIdRecipeId(user.getId(), recipeId)
                .map(userFavoriteRecipes -> {
                    userFavoriteRecipesService.delete(userFavoriteRecipes);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("UserFavoriteRecipes not found with ids "
                        + recipeId + " and " + user.getId()));
    }
}