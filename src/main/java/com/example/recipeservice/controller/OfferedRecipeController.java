package com.example.recipeservice.controller;

import com.example.recipeservice.dto.RecipeModelDTO;
import com.example.recipeservice.exception.ResourceNotFoundException;
import com.example.recipeservice.model.OfferedRecipe;
import com.example.recipeservice.model.RecipeModel;
import com.example.recipeservice.service.ImageService;
import com.example.recipeservice.service.OfferedRecipeService;
import com.example.recipeservice.service.RecipeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api")
@Tag(name = "Контроллер предлагаемых рецептов", description = "Позволяет отображать, предлагать и голосовать за рецепт.")
public class OfferedRecipeController {
    private static final Logger log = LoggerFactory.getLogger(OfferedRecipeController.class);
    @Autowired
    private RecipeService recipeService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private OfferedRecipeService offeredRecipeService;

    @Operation(summary="Отображение предложенных рецептов",
            description="Позволяет получить страницу с рецептами. Доступ - модераторы и администраторы.")
    @GetMapping("/get/offeredRecipe")
    public Page<OfferedRecipe> getOfferedRecipes(Pageable pageable) {
        log.info("Executing OfferedRecipeController.getOfferedRecipes.");
        return offeredRecipeService.findAll(pageable);
    }
    @Operation(summary="Предложенный рецепт по идентификатору",
            description="Позволяет получить предложенный рецепт по идентификатору. Доступ - модераторы и администраторы.")
    @GetMapping("/get/offeredRecipe/{offeredRecipeId}")
    public OfferedRecipe getOfferedRecipe(@PathVariable Long offeredRecipeId) {
        log.info("Executing OfferedRecipeController.getOfferedRecipe.");
        var offeredRecipe = offeredRecipeService.findById(offeredRecipeId);
        if(offeredRecipe.isPresent()) {
            return offeredRecipe.get();
        } else throw new ResourceNotFoundException("OfferedRecipe not found with id " + offeredRecipeId);
    }

    @Operation(summary="Предложение рецепта",
            description="Позволяет предлагать новый рецепт. Доступ - авторизированные пользователи.")
    @PostMapping("/offeredRecipe")
    public OfferedRecipe createOfferedRecipe(@Valid @RequestBody RecipeModelDTO recipeRequest) {
        log.info("Executing OfferedRecipeController.createOfferedRecipe.");
        var recipe = new OfferedRecipe();
        recipe.setCookingTime(recipeRequest.getCookingTime());
        recipe.setDescription(recipeRequest.getDescription());
        recipe.setIngredients(recipeRequest.getIngredients());
        recipe.setName(recipeRequest.getName());
        if(recipeRequest.getImageId() == null) {
            recipe.setImageId(null);
            return offeredRecipeService.save(recipe);
        }
        else return imageService.findById(recipeRequest.getImageId())
                .map(image -> {
                    recipe.setImageId(image);
                    return offeredRecipeService.save(recipe);
                }).orElseThrow(() -> new ResourceNotFoundException("Image not found with id " + recipeRequest.getImageId()));
    }
    @Operation(summary="Проголосовать за принятие рецепта",
            description="Позволяет проголосовать за принятие нового рецепта. Доступ - модераторы и администраторы.")
    @PutMapping("/edit/offeredRecipe/{recipeId}")
    public ResponseEntity<Object> moderatorVoting(@PathVariable Long recipeId, @Valid @RequestBody Boolean moderatorVote) {
        var recipeRequest = new OfferedRecipe();
        var offeredRecipe = offeredRecipeService.findById(recipeId);
        if(offeredRecipe.isPresent())
            recipeRequest = offeredRecipe.get();
        else throw new ResourceNotFoundException("Offered recipe not found with id " + recipeRequest.getImageId());
        if(Boolean.TRUE.equals(moderatorVote)) {
            var recipe = new RecipeModel();
            recipe.setCookingTime(recipeRequest.getCookingTime());
            recipe.setDescription(recipeRequest.getDescription());
            recipe.setIngredients(recipeRequest.getIngredients());
            recipe.setName(recipeRequest.getName());
            recipe.setRanking(0);
            recipe.setVotes(0);
            if(recipeRequest.getImageId() == null) {
                recipe.setImageId(null);
                recipeService.save(recipe);
                offeredRecipeService.delete(recipeRequest);
                return ResponseEntity.ok().build();
            }
            else {
                OfferedRecipe finalRecipeRequest = recipeRequest;
                OfferedRecipe finalRecipeRequest1 = recipeRequest;
                return imageService.findById(recipeRequest.getImageId().getId())
                        .map(image -> {
                            recipe.setImageId(image);
                            recipeService.save(recipe);
                            offeredRecipeService.delete(finalRecipeRequest);
                            return ResponseEntity.ok().build();
                        }).orElseThrow(() -> new ResourceNotFoundException("Image not found with id " + finalRecipeRequest1.getImageId()));
            }
        } else {
            offeredRecipeService.delete(recipeRequest);
            return ResponseEntity.ok().build();
        }
    }

}
