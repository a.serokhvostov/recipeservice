package com.example.recipeservice.controller;

import com.example.recipeservice.auth.UserAuthenticationService;
import com.example.recipeservice.dto.RecipeCommentsDTO;
import com.example.recipeservice.exception.ResourceNotFoundException;
import com.example.recipeservice.model.RecipeComments;
import com.example.recipeservice.model.RecipeModel;
import com.example.recipeservice.service.RecipeCommentsService;
import com.example.recipeservice.service.RecipeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
@Tag(name = "Контроллер комментариев к рецептам",
        description = "Позволяет отображать, производить поиск, загружать и удалять комментарии к рецептам.")
public class RecipeCommentsController {
    static final String RECIPECOMMENTS_NOT_FOUND_WITH_ID = "RecipeComments not found with id ";
    static final String RECIPE_NOT_FOUND_WITH_ID = "Recipe not found with id ";

    @Autowired
    private RecipeCommentsService recipeCommentsService;

    @Autowired
    private UserAuthenticationService userAuthenticationService;

    @Autowired
    private RecipeService recipeService;

    @Operation(summary="Получить комментарии к рецепту.",
            description="Позволяет получить комментарии к рецепту. Доступ - авторизированные пользователи.")
    @GetMapping("/recipeComments/recipe/{recipeId}")
    public Page<RecipeComments> getRecipeComments(@PathVariable Long recipeId, Pageable pageable) {
        RecipeModel recipe;
        if(recipeService.findById(recipeId).isPresent()) {
            recipe = recipeService.findById(recipeId).get();
        } else throw new ResourceNotFoundException(RECIPE_NOT_FOUND_WITH_ID + recipeId);
        return recipeCommentsService.findByRecipeId(recipe, pageable);
    }

    @Operation(summary="Получить комментарий.",
            description="Позволяет получить комментарий по идентификатору. Доступ - авторизированные пользователи.")
    @GetMapping("/recipeComments/{recipeCommentsId}")
    public RecipeComments getRecipeCommentsById(@PathVariable Long recipeCommentsId) {
        var recipeComments = recipeCommentsService.findById(recipeCommentsId);
        if(recipeComments.isPresent())
            return recipeComments.get();
        else throw new ResourceNotFoundException(RECIPECOMMENTS_NOT_FOUND_WITH_ID + recipeCommentsId);

    }

    @Operation(summary="Добавить комментарий.",
            description="Позволяет добавить комментарий к рецепту. Доступ - авторизированные пользователи.")
    @PostMapping("/recipeComments")
    public RecipeComments createRecipeComments(@Valid @RequestBody RecipeCommentsDTO recipeCommentsRequest) {
        var recipeComments = new RecipeComments();
        if(recipeService.findById(recipeCommentsRequest.getRecipeId()).isPresent()) {
            recipeComments.setRecipeId(recipeService.findById(recipeCommentsRequest.getRecipeId()).get());
        } else throw new ResourceNotFoundException(RECIPE_NOT_FOUND_WITH_ID + recipeCommentsRequest.getRecipeId());
        if(userAuthenticationService.findByToken(recipeCommentsRequest.getToken()).isPresent()) {
            recipeComments.setUserId(userAuthenticationService.findByToken(recipeCommentsRequest.getToken()).get());
        } else throw new ResourceNotFoundException("User not found with this token");

        recipeComments.setMessage(recipeCommentsRequest.getMessage());
        recipeComments.setPublicationTime(DateTime.now().toDate());

        return recipeCommentsService.save(recipeComments);
    }

    @Operation(summary="Редактировать комментарий.",
            description="Позволяет отредактировать комментарий. Доступ - модераторы и администраторы.")
    @PutMapping("/edit/recipeComments/{recipeCommentsId}")
    public RecipeComments updateRecipeComments(@PathVariable Long recipeCommentsId,
                                   @Valid @RequestBody RecipeCommentsDTO recipeCommentsRequest) {
        if(!recipeService.findById(recipeCommentsRequest.getRecipeId()).isPresent()) {
            throw new ResourceNotFoundException(RECIPE_NOT_FOUND_WITH_ID + recipeCommentsRequest.getRecipeId());
        }
        if(!userAuthenticationService.findByToken(recipeCommentsRequest.getToken()).isPresent()) {
            throw new ResourceNotFoundException("User not found with this token");
        }

        return recipeCommentsService.findById(recipeCommentsId)
                .map(recipeComments -> {
                    recipeComments.setMessage(recipeCommentsRequest.getMessage());
                    recipeComments.setRecipeId(recipeService.findById(recipeCommentsRequest.getRecipeId()).get());
                    recipeComments.setPublicationTime(DateTime.now().toDate());
                    recipeComments.setUserId(userAuthenticationService.findByToken(recipeCommentsRequest.getToken()).get());
                    return recipeCommentsService.save(recipeComments);
                }).orElseThrow(() -> new ResourceNotFoundException(RECIPECOMMENTS_NOT_FOUND_WITH_ID + recipeCommentsId));
    }

    @Operation(summary="Удалить комментарий.",
            description="Позволяет удалить комментарий. Доступ - администраторы.")
    @DeleteMapping("/delete/recipeComments/{recipeCommentsId}")
    public ResponseEntity<Object> deleteRecipeComments(@PathVariable Long recipeCommentsId) {
        return recipeCommentsService.findById(recipeCommentsId)
                .map(recipeComments -> {
                    recipeCommentsService.delete(recipeComments);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException(RECIPECOMMENTS_NOT_FOUND_WITH_ID + recipeCommentsId));
    }
}