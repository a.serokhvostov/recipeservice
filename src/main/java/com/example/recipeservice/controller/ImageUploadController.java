package com.example.recipeservice.controller;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import com.example.recipeservice.exception.ResourceNotFoundException;
import com.example.recipeservice.model.ImageModel;
import com.example.recipeservice.service.ImageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
@CrossOrigin
@RequestMapping("/api/public/image")
@Tag(name = "Контроллер изображений", description = "Позволяет загружать и получать изображения.")
public class ImageUploadController {
    private static final Logger log = LoggerFactory.getLogger(ImageUploadController.class);

    @Autowired
    ImageService imageService;

    @Operation(summary="Загрузка изображений", description="Позволяет загружать изображения в формате jpeg.")
    @PostMapping("/upload")
    public ResponseEntity<String> uploadImage(@RequestParam("imageFile") @Parameter(description = "Файл в формате jpeg")
                                                          MultipartFile file) throws IOException {
        log.info("Original Image Byte Size - {}.", file.getBytes().length);
        var img = new ImageModel(file.getOriginalFilename(), file.getContentType(),
                compressBytes(file.getBytes()));
        imageService.save(img);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary="Просмотр изображения", description="Позволяет получить изображение по его имени.")
    @GetMapping(path = { "/get/{imageName}" })
    public ImageModel getImage(@PathVariable("imageName") @Parameter(description = "Имя изображения в базе")
                                           String imageName) {
        final Optional<ImageModel> retrievedImage = imageService.findByName(imageName);
        if(retrievedImage.isPresent()) {
            var image = retrievedImage.get();
            image.setPicByte(decompressBytes(image.getPicByte()));
            return image;
        } else throw new ResourceNotFoundException("Image not found with name " + imageName);
    }
    @GetMapping(path = { "/get/id/{imageId}" })
    public ImageModel getImageById(@PathVariable("imageId")
                                       Long imageId) {
        final Optional<ImageModel> retrievedImage = imageService.findById(imageId);
        if(retrievedImage.isPresent()) {
            var image = retrievedImage.get();
            image.setPicByte(decompressBytes(image.getPicByte()));
            return image;
        } else throw new ResourceNotFoundException("Image not found with id " + imageId);
    }
        // compress the image bytes before storing it in the database
    public static byte[] compressBytes(byte[] data) {
        var deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();
        var outputStream = new ByteArrayOutputStream(data.length);
        var buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            log.info("IOException occurred in ImageUploadController.compressBytes.");
        }
        log.info("Compressed Image Byte Size - {}.", outputStream.toByteArray().length);
        return outputStream.toByteArray();
    }

        // uncompress the image bytes before returning it to the angular application
    public static byte[] decompressBytes(byte[] data) {
        var inflater = new Inflater();
        inflater.setInput(data);
        var outputStream = new ByteArrayOutputStream(data.length);
        var buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException ioe) {
            log.info("IOException occurred in ImageUploadController.decompressBytes.");
        } catch (DataFormatException e) {
            log.info("DataFormatException occurred in ImageUploadController.decompressBytes.");
        }
        return outputStream.toByteArray();
    }
}