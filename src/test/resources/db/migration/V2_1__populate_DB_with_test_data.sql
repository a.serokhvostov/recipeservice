INSERT INTO image_table (id, name, pic_byte, type) values (12, 'test_name1', E'\\336\\255\\276\\357'::bytea, 'test_type1');
INSERT INTO image_table (id, name, pic_byte, type) values (15, 'test_name2', E'\\326\\255\\276\\357'::bytea, 'test_type2');
INSERT INTO image_table (id, name, pic_byte, type) values (16, 'test_name3', E'\\332\\252\\243\\352'::bytea, 'test_type3');
INSERT INTO image_table (id, name, pic_byte, type) values (17, 'test_name4', E'\\332\\251\\243\\352'::bytea, 'test_type4');
INSERT INTO image_table (id, name, pic_byte, type) values (18, 'test_name5', E'\\332\\151\\243\\352'::bytea, 'test_type5');
INSERT INTO recipe (id, name, cooking_time, ingredients, description, ranking, votes, image_id) values (3, 'salad', 4, 'salad, galat', 'salad', 0, 0, 12);
INSERT INTO recipe (id, name, cooking_time, ingredients, description, ranking, votes, image_id) values (4, 'salad', 3, 'salad, galat', 'salad', 0, 0, 16);
INSERT INTO recipe (id, name, cooking_time, ingredients, description, ranking, votes, image_id) values (6, 'salad', 2, 'salad, galat', 'salad', 0, 0, 18);
INSERT INTO recipe_comments (id, message, publication_time, user_id, recipe_id) values (21, 'test_update', '04:05:06', 111, 38);
INSERT INTO recipe_comments (id, message, publication_time, user_id, recipe_id) values (22, 'test_delete', '04:05:07', 112, 37);
INSERT INTO offered_recipe (id, name, cooking_time, ingredients, description) values (51, 'porridge', 4, 'semolina, milk', 'porridge description');
INSERT INTO offered_recipe (id, name, cooking_time, ingredients, description) values (52, 'LAGGAGE', 4, 'test, test', 'test description');
INSERT INTO user_favorite (user_id, recipe_id) values (111, 38);
INSERT INTO user_favorite (user_id, recipe_id) values (111, 36);