package com.example.recipeservice.test.unit;

import com.example.recipeservice.test.AbstractTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
class ImageUploadControllerTests implements AbstractTest {
    private static final Logger log = LoggerFactory.getLogger(ImageUploadControllerTests.class);
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .addFilter(springSecurityFilterChain).build();
    }

    @Test
    void controllerContextLoads() {
        log.info("Executing test: ImageUploadControllerTests.controllerContextLoad.");
        assertThat(mockMvc, notNullValue());
    }

    @Test
    void reactsOnImageUploadRequest() throws Exception {
        log.info("Executing test: ImageUploadControllerTests.reactsOnImageUploadRequest.");
        MockMultipartFile file
                = new MockMultipartFile(
                "imageFile",
                "imageFile.jpg",
                MediaType.IMAGE_JPEG_VALUE,
                "Hello, World!".getBytes()
        );
        mockMvc.perform( MockMvcRequestBuilders
                .multipart("/api/public/image/upload").file(file)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void reactsOnGetImageRequest() throws Exception {
        log.info("Executing test: ImageUploadControllerTests.reactsOnGetImageRequest.");
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/public/image/get/test_name4")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    void reactsOnGetImageByIdRequest() throws Exception {
        log.info("Executing test: ImageUploadControllerTests.reactsOnGetImageByIdRequest.");
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/public/image/get/id/12")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }



}
