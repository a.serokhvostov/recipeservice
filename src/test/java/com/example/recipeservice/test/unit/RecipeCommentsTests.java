package com.example.recipeservice.test.unit;

import com.example.recipeservice.dto.RecipeCommentsDTO;
import com.example.recipeservice.test.AbstractTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.example.recipeservice.test.util.TestUtilities.asJsonString;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static com.example.recipeservice.test.util.TestUtilities.*;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
class RecipeCommentsTests implements AbstractTest {
    private static final Logger log = LoggerFactory.getLogger(RecipeCommentsTests.class);
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .addFilter(springSecurityFilterChain).build();
    }

    @Test
    void reactsOnGetByRecipeRequestWithAuthorization() throws Exception {
        log.info("Executing test: RecipeCommentsTests.reactsOnGetByRecipeRequestWithAuthorization.");
        String token = obtainAccessToken("user", "user", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/recipeComments/recipe/38")
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    void reactsOnGetByIdRequestWithAuthorization_WithRandomId() throws Exception {
        log.info("Executing test: RecipeCommentsTests.reactsOnGetByIdRequestWithAuthorization.");
        String token = obtainAccessToken("user", "user", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/recipeComments/10000")
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void reactsOnGetByIdRequestWithAuthorization_WithExactId() throws Exception {
        log.info("Executing test: RecipeCommentsTests.reactsOnGetByIdRequestWithAuthorization.");
        String token = obtainAccessToken("user", "user", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/recipeComments/21")
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void reactsOnCreateRequestWithAuthorization() throws Exception {
        log.info("Executing test: RecipeCommentsTests.reactsOnCreateRequestWithAuthorization.");
        String token = obtainAccessToken("user", "user", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders
                .post("/api/recipeComments")
                .header("Authorization", "Bearer "
                        + token)
                .content(asJsonString(new RecipeCommentsDTO("test", 38L, token)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void reactsOnUpdateRequestWithAuthorization_WithRandomId() throws Exception
    {
        log.info("Executing test: RecipeCommentsTests.reactsOnUpdateRequestWithAuthorization.");
        String token = obtainAccessToken("moderator", "moderator", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders
                .put("/api/edit/recipeComments/1000")
                .header("Authorization", "Bearer "
                        + token)
                .content(asJsonString(new RecipeCommentsDTO("test", 38L, token)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
    @Test
    void reactsOnUpdateRequestWithAuthorization_WithExactId() throws Exception
    {
        log.info("Executing test: RecipeCommentsTests.reactsOnUpdateRequestWithAuthorization.");
        String token = obtainAccessToken("moderator", "moderator", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders
                .put("/api/edit/recipeComments/21")
                .header("Authorization", "Bearer "
                        + token)
                .content(asJsonString(new RecipeCommentsDTO("test", 38L, token)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void reactsOnDeleteRequestWithAuthorization_WithRandomId() throws Exception
    {
        log.info("Executing test: RecipeCommentsTests.reactsOnDeleteRequestWithAuthorization.");
        mockMvc.perform( MockMvcRequestBuilders.delete("/api/delete/recipeComments/555")
                .header("Authorization", "Bearer "
                        + obtainAccessToken("admin", "admin", mockMvc)))
                .andExpect(status().isNotFound());
    }

    @Test
    void reactsOnDeleteRequestWithAuthorization_WithExactId() throws Exception
    {
        log.info("Executing test: RecipeCommentsTests.reactsOnDeleteRequestWithAuthorization.");
        mockMvc.perform( MockMvcRequestBuilders.delete("/api/delete/recipeComments/22")
                .header("Authorization", "Bearer "
                        + obtainAccessToken("admin", "admin", mockMvc)))
                .andExpect(status().isOk());
    }


}
