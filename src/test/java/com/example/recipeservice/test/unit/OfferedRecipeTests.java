package com.example.recipeservice.test.unit;

import com.example.recipeservice.dto.RecipeModelDTO;
import com.example.recipeservice.test.AbstractTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static com.example.recipeservice.test.util.TestUtilities.*;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
class OfferedRecipeTests implements AbstractTest {
    private static final Logger log = LoggerFactory.getLogger(OfferedRecipeTests.class);
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .addFilter(springSecurityFilterChain).build();
    }
    @Test
    void reactsOnGetRequestWithAuthorization() throws Exception {
        log.info("Executing test: OfferedRecipeTests.reactsOnGetRequestWithAuthorization.");
        String token = obtainAccessToken("moderator", "moderator", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/get/offeredRecipe/")
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void reactsOnGetByIdRequestWithAuthorization_WithRandomId() throws Exception {
        log.info("Executing test: OfferedRecipeTests.reactsOnGetByIdRequestWithAuthorization.");
        String token = obtainAccessToken("moderator", "moderator", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/get/offeredRecipe/2000")
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
    @Test
    void reactsOnGetByIdRequestWithAuthorization_WithExactId() throws Exception {
        log.info("Executing test: OfferedRecipeTests.reactsOnGetByIdRequestWithAuthorization.");
        String token = obtainAccessToken("moderator", "moderator", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/get/offeredRecipe/51")
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
    @Test
    void reactsOnCreateRequestWithAuthorization() throws Exception {
        log.info("Executing test: OfferedRecipeTests.reactsOnCreateRequestWithAuthorization.");
        mockMvc.perform( MockMvcRequestBuilders
                .post("/api/offeredRecipe")
                .header("Authorization", "Bearer "
                        + obtainAccessToken("user", "user", mockMvc))
                .content(asJsonString(new RecipeModelDTO("slad", 2,
                        "ccccc", "dsf", 15L)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void reactsOnUpdateRequestWithAuthorization_WithRandomId() throws Exception
    {
        log.info("Executing test: OfferedRecipeTests.reactsOnUpdateRequestWithAuthorization.");
        mockMvc.perform( MockMvcRequestBuilders
                .put("/api/edit/offeredRecipe/3333")
                .header("Authorization", "Bearer "
                        + obtainAccessToken("moderator", "moderator", mockMvc))
                .content(asJsonString(true))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
    @Test
    void reactsOnUpdateRequestWithAuthorization_WithExactId_WithPositiveDecision() throws Exception
    {
        log.info("Executing test: OfferedRecipeTests.reactsOnUpdateRequestWithAuthorization.");
        mockMvc.perform( MockMvcRequestBuilders
                .put("/api/edit/offeredRecipe/51")
                .header("Authorization", "Bearer "
                        + obtainAccessToken("moderator", "moderator", mockMvc))
                .content(asJsonString(true))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    @Test
    void reactsOnUpdateRequestWithAuthorization_WithExactId_WithNegativeDecision() throws Exception
    {
        log.info("Executing test: OfferedRecipeTests.reactsOnUpdateRequestWithAuthorization.");
        mockMvc.perform( MockMvcRequestBuilders
                .put("/api/edit/offeredRecipe/52")
                .header("Authorization", "Bearer "
                        + obtainAccessToken("moderator", "moderator", mockMvc))
                .content(asJsonString(false))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }



}
