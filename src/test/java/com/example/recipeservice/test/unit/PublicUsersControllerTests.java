package com.example.recipeservice.test.unit;

import com.example.recipeservice.test.AbstractTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
class PublicUsersControllerTests implements AbstractTest {
    private static final Logger log = LoggerFactory.getLogger(PublicUsersControllerTests.class);
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .addFilter(springSecurityFilterChain).build();
    }

    @Test
    void reactsOnRegistration() throws Exception {
        log.info("Executing test: PublicUsersControllerTests.reactsOnRegistration.");
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("username", "test");
        params.add("password", "test");
        ResultActions result
                = mockMvc.perform(MockMvcRequestBuilders
                .post("/api/public/users/register")
                .params(params)
                .accept("application/json;charset=UTF-8"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));
    }
    @Test
    void reactsOnFindByToken_WithWrongToken() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("token", "wrong_token");
        ResultActions result
                = mockMvc.perform(MockMvcRequestBuilders
                .post("/api/public/users/find_by_token")
                .params(params)
                .accept("application/json;charset=UTF-8"))
                .andExpect(status().isNotFound());
    }

}
