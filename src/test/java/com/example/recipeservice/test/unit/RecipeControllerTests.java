package com.example.recipeservice.test.unit;

import com.example.recipeservice.dto.RecipeModelDTO;
import com.example.recipeservice.test.AbstractTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static com.example.recipeservice.test.util.TestUtilities.*;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
class RecipeControllerTests implements AbstractTest{
    private static final Logger log = LoggerFactory.getLogger(RecipeControllerTests.class);
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .addFilter(springSecurityFilterChain).build();
    }

    @Test
    void controllerContextLoads(){
        log.info("Executing test: RecipeControllerTests.controllerContextLoad.");
        assertThat(mockMvc, notNullValue());
    }

    @Test
    void reactsOnGetRecipesRequest() throws Exception {
        log.info("Executing test: RecipeControllerTests.reactsOnGetRecipesRequest.");
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/public/recipe")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void reactsOnGetRecipesByNameRequest() throws Exception {
        log.info("Executing test: RecipeControllerTests.reactsOnGetRecipesByNameRequest.");
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/public/recipe/salad")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void reactsOnGetRecipesByIdRequest() throws Exception {
        log.info("Executing test: RecipeControllerTests.reactsOnGetRecipesByNameRequest.");
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/public/recipe/id/38")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void reactsOnGetRecipesByIngredientsRequest() throws Exception {
        log.info("Executing test: RecipeControllerTests.reactsOnGetRecipesByIngredientsRequest.");
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/public/recipe/ingredients/salad")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void reactsOnCreateRecipeRequestWithAuthorization() throws Exception {
        log.info("Executing test: RecipeControllerTests.reactsOnCreateRecipeRequest.");
        mockMvc.perform( MockMvcRequestBuilders
                .post("/api/create/recipe")
                .header("Authorization", "Bearer "
                        + obtainAccessToken("admin", "admin", mockMvc))
                .content(asJsonString(new RecipeModelDTO("slad", 2,
                        "ccccc", "dsf", 15L)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void reactsOnUpdateRecipeRequestWithAuthorization() throws Exception
    {
        log.info("Executing test: RecipeControllerTests.reactsOnUpdateRecipeRequest.");
        mockMvc.perform( MockMvcRequestBuilders
                .put("/api/edit/recipe/{id}", 3)
                .header("Authorization", "Bearer "
                        + obtainAccessToken("moderator", "moderator", mockMvc))
                .content(asJsonString(new RecipeModelDTO("sald", 2,
                        "sald", "sald", 12L)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    @Test
    void reactsOnRateRecipeRequestWithAuthorization() throws Exception
    {
        log.info("Executing test: RecipeControllerTests.reactsOnUpdateRecipeRequest.");
        mockMvc.perform( MockMvcRequestBuilders
                .put("/api/recipe/rate/{id}", 37)
                .header("Authorization", "Bearer "
                        + obtainAccessToken("user", "user", mockMvc))
                .content(asJsonString(5))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void reactsOnDeleteRecipeRequestWithAuthorization() throws Exception
    {
        log.info("Executing test: RecipeControllerTests.reactsOnDeleteRecipeRequest.");
        mockMvc.perform( MockMvcRequestBuilders.delete("/api/delete/recipe/{id}", 4)
                .header("Authorization", "Bearer "
                        + obtainAccessToken("admin", "admin", mockMvc)))
                .andExpect(status().isOk());
    }
}
