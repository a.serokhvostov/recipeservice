package com.example.recipeservice.test.unit;

import com.example.recipeservice.dto.UserFavoriteRecipesDTO;
import com.example.recipeservice.test.AbstractTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.example.recipeservice.test.util.TestUtilities.asJsonString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static com.example.recipeservice.test.util.TestUtilities.*;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
class UserFavoriteRecipesTests implements AbstractTest {
    private static final Logger log = LoggerFactory.getLogger(UserFavoriteRecipesTests.class);
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private MockMvc mockMvc;




    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .addFilter(springSecurityFilterChain).build();
    }
    @Test
    void controllerContextLoads(){
        log.info("Executing test: UserFavoriteRecipesTests.controllerContextLoad.");
        assertThat(mockMvc, notNullValue());
    }

    @Test
    void reactsOnGetRequestWithAuthorization() throws Exception {
        log.info("Executing test: UserFavoriteRecipesTests.reactsOnGetRequestWithAuthorization.");
        String token = obtainAccessToken("user", "user", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/userFavoriteRecipes/" + token)
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    void reactsOnGetByIdRequestWithAuthorization_WithRandomId() throws Exception {
        log.info("Executing test: UserFavoriteRecipesTests.reactsOnGetByIdRequestWithAuthorization.");
        String token = obtainAccessToken("user", "user", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/userFavoriteRecipes/10000/"+ token)
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
    @Test
    void reactsOnGetByIdRequestWithAuthorization_WithExactId() throws Exception {
        log.info("Executing test: UserFavoriteRecipesTests.reactsOnGetByIdRequestWithAuthorization.");
        String token = obtainAccessToken("user", "user", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/userFavoriteRecipes/36/"+ token)
                .header("Authorization", "Bearer " + token)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    void reactsOnCreateRequestWithAuthorization() throws Exception {
        log.info("Executing test: UserFavoriteRecipesTests.reactsOnCreateRequestWithAuthorization.");
        String token = obtainAccessToken("user", "user", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders
                .post("/api/userFavoriteRecipes")
                .header("Authorization", "Bearer "
                        + token)
                .content(asJsonString(new UserFavoriteRecipesDTO(36L, token)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    @Test
    void reactsOnDeleteRequestWithAuthorization_WithRandomId() throws Exception
    {
        log.info("Executing test: RecipeControllerTests.reactsOnDeleteRecipeRequest.");
        String token = obtainAccessToken("user", "user", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders.delete("/api/userFavoriteRecipes/100/"
                + token, 4)
                .header("Authorization", "Bearer "
                        + token))
                .andExpect(status().isNotFound());
    }
    @Test
    void reactsOnDeleteRequestWithAuthorization_WithExactId() throws Exception
    {
        log.info("Executing test: RecipeControllerTests.reactsOnDeleteRecipeRequest.");
        String token = obtainAccessToken("user", "user", mockMvc);
        mockMvc.perform( MockMvcRequestBuilders.delete("/api/userFavoriteRecipes/38/"
                + token, 4)
                .header("Authorization", "Bearer "
                        + token))
                .andExpect(status().isOk());
    }
}
