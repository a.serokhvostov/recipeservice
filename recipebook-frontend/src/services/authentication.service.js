import { BehaviorSubject } from 'rxjs';

const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));

export const authenticationService = {
    login,
    logout,
    register,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue() { return currentUserSubject.value }
};

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({})
    };

    return fetch(`/api/public/users/login?username=`.concat(username).concat('&password=').concat(password), requestOptions)
        .then(response => response.text())
        .then(token => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            return fetch(`/api/public/users/find_by_token?token=`.concat(token), requestOptions)
            .then(response => response.json())
            .then(role => { 
                console.log(role.authorities[0].authority)
                localStorage.setItem('currentUser', JSON.stringify({token: token, role: role.authorities[0].authority}));
                currentUserSubject.next({token: token, role: role.authorities[0].authority});
                return currentUserSubject.value;
            })
        });
}
function register(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({})
    };

    return fetch(`/api/public/users/register?username=`.concat(username).concat('&password=').concat(password), requestOptions)
        .then(response => response.text())
        .then(answer => console.log(answer))

}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    currentUserSubject.next(null);
}
