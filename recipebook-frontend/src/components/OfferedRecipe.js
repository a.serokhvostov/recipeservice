import React from 'react'
import { useState, useEffect, useCallback, useRef } from 'react'
import { useFormik } from 'formik'
import { authenticationService } from '../services/authentication.service'


const OfferedRecipe = () => {
    const [newRecipe, setNewRecipe] = useState()
    const recipeForm = useRef(null)
    const sendRecipe = (values) => {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': authenticationService.currentUserValue.token
            },
            body: JSON.stringify(values)
        };

        return fetch(`/api/offeredRecipe`, requestOptions)
            .then(response => response.json()).then(val => console.log(val))
    }


    const formik = useFormik({
        initialValues: {
            name: '',
            cookingTime: '',
            ingredients: '',
            description: '',
            imageId: ''
        },
        onSubmit: values => {
            sendRecipe(values)
        },
    });
    return (
        <div>
            <h2>
                предложить рецепт
            </h2>
            <form onSubmit={formik.handleSubmit}>
                <label htmlFor="name">Name</label>
                <input
                    id="name"
                    name="name"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.name}
                />
                <label htmlFor="cookingTime">Cooking time</label>
                <input
                    id="cookingTime"
                    name="cookingTime"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.cookingTime}
                />
                <label htmlFor="ingredients">Ingredients</label>
                <input
                    id="ingredients"
                    name="ingredients"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.ingredients}
                />
                <label htmlFor="description">Description</label>
                <input
                    id="description"
                    name="description"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.description}
                />

                <input type="submit" value="отправить рецепт" />
            </form>

        </div>
    )
}

export default OfferedRecipe
