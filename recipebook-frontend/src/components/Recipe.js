import { React, useState, useEffect, useCallback } from 'react'
import { authenticationService } from '../services/authentication.service'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

function Recipe({ recipe, sendVote, sendComment }) {
    const fetchRecipeComment = useCallback(async (id) => {
        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': authenticationService.currentUserValue.token
            }
        };

        return fetch(`/api/recipeComments/recipe/${recipe.id}`, requestOptions)
            .then(response => response.json())
            .then(comments => {
                comments = comments.content
                console.log(JSON.stringify(comments, null, 4))
                return [...comments]
            })
    }
        , []);

    const [comment, setComment] = useState("initialState")
    const [commentUpd, setCommentUpd] = useState(false)
    const [commentArray, setCommentArray] = useState([])
    useEffect(() => {
        if (commentUpd === false) {
            setCommentUpd(true)
            fetchRecipeComment(recipe.id).then((upd_comments) =>
                setCommentArray(upd_comments)
            )
        }
    }, [commentUpd])
    const deleteComment = (values) => {
        const requestOptions = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': authenticationService.currentUserValue.token
            },
            body: JSON.stringify({recipeCommentsId: values})
        };

        return fetch(`/api/delete/recipeComments/${values}`, requestOptions)
            .then(response => response.text()).then(val => {setCommentUpd(false)})
    }
    return (
        <div className="recipe" >
            <h2>{recipe.name}</h2>
            <h2>Cooking time : {recipe.cookingTime}</h2>
            <h2>Ingredients : {recipe.ingredients}</h2>
            <img style={{ float: "left" }} src={recipe.imageSrc} alt="dish" />
            {recipe.description.slice(0, 100)}

            <p>{Array.apply(null, Array(parseInt(recipe.ranking))).map((el, i) => "⭐")}</p>
            <h2>votes : {recipe.votes}</h2>
            <input type="button" onClick={() => sendVote(0)} value="0" />
            <input type="button" onClick={() => sendVote(1)} value="1" />
            <input type="button" onClick={() => sendVote(2)} value="2" />
            <input type="button" onClick={() => sendVote(3)} value="3" />
            <input type="button" onClick={() => sendVote(4)} value="4" />
            <input type="button" onClick={() => sendVote(5)} value="5" />
            <div>

                <textarea onChange={(e) => setComment(e.target.value)} value={comment} />
                <input type="button" onClick={() => sendComment(comment).then((comment) => setCommentUpd(false))} value="Отправить" />
            </div>

            <div className="comments">
                {commentArray.map((comment, index) => <div style={{ border: "solid 1px red" }}>
                    <h2>{comment.message}</h2>
                    <h2>User : {comment.userId.username}</h2>
                    <h2>Publication time : {comment.publicationTime}</h2>
                    {["ROLE_ADMIN"].includes(authenticationService.currentUserValue?.role) ?
  <input className="fetch-button" value="Удалить комментарий" type="button" onClick={() => deleteComment(comment.id)} />
: ""}
                </div>)}
                {commentArray.length ? "" : "Комментариев пока нет"}
            </div>
        </div>
    )
}

export default Recipe
