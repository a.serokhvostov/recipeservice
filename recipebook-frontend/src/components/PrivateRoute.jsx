import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { authenticationService } from '../services/authentication.service';

export const PrivateRoute = ({ component: Component, role=[], ...rest }) => (
    <Route {...rest} render={props => {
        const currentUser = authenticationService.currentUserValue;
        if (!(currentUser)) {
            // not logged in so redirect to login page with the return url
            return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        }
        
        if(role.length > 0 && !role.includes(currentUser.role))
            return <div>you dont have permissions</div>
        // authorised so return component
        return <div>
            <Component {...props} />
        </div>
    }} />
)