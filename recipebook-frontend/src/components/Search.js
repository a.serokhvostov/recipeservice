import React from 'react'
import {useState} from 'react'

function Search({fetchDataRecipeName, fetchDataRecipeIngredient}) {

    const [recipeSearchWord, setRecipeSearchWord] = useState('');
    const [ingredSearchWord, setIngredSearchWord] = useState('');

    
    return (
        <div>
            <input key="name_input" className="fetch-button" value={recipeSearchWord} onChange={(event) => { setRecipeSearchWord(event.target.value) }} />
            <input className="fetch-button" type="button" value="поиск по названию" onClick={() => fetchDataRecipeName(recipeSearchWord)} /> <br />
            <input key="ingred_input" className="fetch-button" value={ingredSearchWord} onChange={(event) => { setIngredSearchWord(event.target.value) }} />
            <input className="fetch-button" type="button" value="поиск по ингредиентам" onClick={() => fetchDataRecipeIngredient(ingredSearchWord)} />

        </div>
    )
}

export default Search
