import React from 'react'
import PropTypes from 'prop-types'


function RecipePreview({ name, imageSrc, description, ranking }) {
    return (
        <div className="recipe" >
            {/* <h3>Book {index + 1}</h3> */}
            <h2>{name}</h2>
            <img style= {{float: "left"}} src={imageSrc} alt="preview" />
             {description.slice(0, 100)}

            <p>{Array.apply(null, Array(parseInt(ranking))).map( (el, i) => "⭐" )}</p>

        </div>
    )
}

RecipePreview.propTypes = {
    name: PropTypes.string,
    imageSrc: PropTypes.string,
    description: PropTypes.string,
    rankin: PropTypes.number
}


export default RecipePreview
