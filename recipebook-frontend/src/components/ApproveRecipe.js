import React from 'react'
import { useState, useEffect, useCallback } from 'react'
import axios from 'axios'
import { authenticationService } from '../services/authentication.service'


function ApproveRecipe() {
    const [recipes, setRecipes] = useState(null)
    const [recipeExist, setRecipeExist] = useState(false)
    const fetchDataRecipeName = useCallback(async () => {


        const requestOptions = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': authenticationService.currentUserValue.token
            }
        };

        fetch(`/api/get/offeredRecipe`, requestOptions)
            .then(response => response.json()).then(response => {

                console.log(JSON.stringify(response.content, null, 2))
                setRecipes(response.content);


            })

    }, [setRecipes]);
    useEffect(() => {
        if (recipeExist === false) {
            setRecipeExist(true)
            fetchDataRecipeName()

        }
    }, [recipes, recipeExist, fetchDataRecipeName])
    const approveRecipe = (values) => {
        const requestOptions = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': authenticationService.currentUserValue.token
            },
            body: JSON.stringify(values)
        };

        return fetch(`/api/edit/offeredRecipe/${values}`, requestOptions)
            .then(response => response.json()).then(val => console.log(val))
    }
    return (
        <div>
            {recipes?.map((recipe, index) => (
                <div className="recipe">
                    <h2>{recipe.name}</h2>
            <h2>Cooking time : {recipe.cookingTime}</h2>
            <h2>Ingredients : {recipe.ingredients}</h2>
            <input type="button" onClick={() => approveRecipe(recipe.id)} value="Одобрить" />
                </div>

            ))}
        </div>
    )
}

export default ApproveRecipe
