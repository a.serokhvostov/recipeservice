import './styles.css'
import RecipePreview from './components/RecipePreview'
import Recipe from './components/Recipe'
import { useState, useEffect, useCallback } from 'react'
import axios from 'axios'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import { PrivateRoute } from './components/PrivateRoute'
import { LoginPage } from './LoginPage'
import { authenticationService } from './services/authentication.service'
import { createBrowserHistory } from 'history';
import Search from './components/Search'
import OfferedRecipe from './components/OfferedRecipe'
import ApproveRecipe from './components/ApproveRecipe'


function App() {
  const [recipes, setRecipes] = useState(null)
  const [recipeExist, setRecipeExist] = useState(false)

  const [currentRecipe, setcurrentRecipe] = useState(null)
  const [currentUser, setCurrentUser] = useState(null);


  const fetchDataRecipeName = useCallback(async (recipeSearchWord) => {
    const response = await axios.get(
      `/api/public/recipe/${recipeSearchWord}`
    );
    console.log(JSON.stringify(response.data, null, 2))
    setRecipes(response.data.content);
  }, [setRecipes]);

  const fetchDataRecipeIngredient = async (ingredSearchWord) => {
    const response = await axios.get(
      `/api/public/recipe/ingredients/${ingredSearchWord}`
    );
    console.log(JSON.stringify(response.data, null, 2))
    setRecipes(response.data.content);
  };

  const getImage = (img_id) => {
    return img_id === 100 ? "100.jpg" : ""
  }
  useEffect(() => {
    if (recipeExist === false) {
      setRecipeExist(true)
      fetchDataRecipeName('')
      authenticationService.currentUser.subscribe(x => setCurrentUser(x));

    }
  }, [recipes, recipeExist, fetchDataRecipeName])
  const logout = () => {
    authenticationService.logout();
    console.log(currentUser)
    createBrowserHistory().push('/login');
  }
  const sendVote = (rank) => {
    const requestOptions = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authenticationService.currentUserValue.token
      },
      body: JSON.stringify(rank)
    };

    return fetch(`/api/recipe/rate/${currentRecipe.id}`, requestOptions)
      .then(response => response.json())
      .then(recipe => {
        console.log(recipe)
        setcurrentRecipe(recipe)
      })
  }
  const sendComment = (comment) => {
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authenticationService.currentUserValue.token
      },
      body: JSON.stringify({
        "message": comment,
        "recipeId": currentRecipe.id,
        "token": authenticationService.currentUserValue.token
      })
    };

    return fetch(`/api/recipeComments`, requestOptions)
      .then(response => response.json())
  }

  return (
    <div className="App">
      <div>
        role: {authenticationService.currentUserValue?.role}
      </div>
      <Router >
        <Link to="/">
          <input type="button" value="back" className="fetch-button" />
        </Link>
        {authenticationService.currentUserValue?.role ?
          <Link to="/offerrecipe">
            <input className="fetch-button" value="Предложить рецепт" type="button" />
          </Link>
          : ""
        }
        
        {["ROLE_ADMIN", "ROLE_MODERATOR"].includes(authenticationService.currentUserValue?.role) ?

          <Link to="/approverecipe">
            <input className="fetch-button" value="Одобрить рецепты" type="button" />
          </Link>
          : ""}

        <input onClick={logout} type="button" className="fetch-button" value="logout" />
        <Route path="/login" component={LoginPage} />
        <PrivateRoute path="/offerrecipe" component={OfferedRecipe} />
        <PrivateRoute path="/approverecipe" role={["ROLE_ADMIN", "ROLE_MODERATOR"]} component={ApproveRecipe} />
        <PrivateRoute path="/recipe" component={
          () =>
            <div>



              <div className="recipes" >

                {currentRecipe &&

                  <Recipe recipe={{ ...currentRecipe, imageSrc: getImage(currentRecipe.imageId) }} sendVote={sendVote} sendComment={sendComment} />
                }
              </div>
            </div>

        } />
        <PrivateRoute exact path="/" component={() => {
          return (<div key="editor_container">
            <Search fetchDataRecipeName={fetchDataRecipeName} fetchDataRecipeIngredient={fetchDataRecipeIngredient} />
            {/* <input key="name_input" className="fetch-button" value={recipeSearchWord} onChange={(event) => { setRecipeSearchWord(event.target.value) }} />
            <input className="fetch-button" type="button" value="поиск по названию" onClick={() => fetchDataRecipeName()} /> <br />
            <input key="ingred_input" className="fetch-button" value={ingredSearchWord} onChange={(event) => { setIngredSearchWord(event.target.value) }} />
            <input className="fetch-button" type="button" value="поиск по ингредиентам" onClick={() => fetchDataRecipeIngredient()} /> */}


            {recipes?.map((recipe, index) => (

              <Link key={recipe.id + "link"} to="/recipe" onClick={() => { setcurrentRecipe(recipe) }}>

                <RecipePreview key={recipe.id} {...{ ...recipe, imgSrc: "lalla" }} />

              </Link>
            ))}

          </div>)
        }} />

      </Router>
    </div>
  );
}

export default App;
