import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";

import { authenticationService } from "../services/authentication.service";

class LoginPage extends React.Component {
  constructor(props) {
    super(props);

    // redirect to home if already logged in
    if (authenticationService.currentUserValue) {
      this.props.history.push("/");
    }
  }
  state = { isLoginView: true };
  render() {
    return (
      <div>
        <h2>Login</h2>
        <Formik
          initialValues={{
            username: "",
            password: "",
          }}
          validationSchema={Yup.object().shape({
            username: Yup.string().required("Username is required"),
            password: Yup.string().required("Password is required"),
          })}
          onSubmit={({ username, password }, { setStatus, setSubmitting }) => {
            setStatus();
            if (this.state.isLoginView) {
              authenticationService.login(username, password).then(
                (user) => {
                  console.log("login success!");
                  console.log(JSON.stringify(user))
                  const { from } = this.props.location.state || {
                    from: { pathname: "/" },
                  };
                  this.props.history.push("/");
                },
                (error) => {
                  setSubmitting(false);
                  setStatus(error);
                }
              );
            } else {
              authenticationService.register(username, password).then(
                () => {
                  this.setState({ isLoginView: true });
                  setSubmitting(false);
                },
                (error) => {
                  setSubmitting(false);
                  setStatus(error);
                }
              );
            }
          }}
        >
          {({ errors, status, touched, isSubmitting }) => (
            <Form>
              <div className="form-group">
                <label htmlFor="username">Username</label>
                <Field
                  name="username"
                  type="text"
                  className={
                    "form-control" +
                    (errors.username && touched.username ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="username"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <Field
                  name="password"
                  type="password"
                  className={
                    "form-control" +
                    (errors.password && touched.password ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="password"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <button
                  type="submit"
                  className="btn btn-primary"
                  disabled={isSubmitting}
                >
                  {this.state.isLoginView ? "Войти" : "Зарегистрироваться"}
                </button>
                <button
                  type="button"
                  onClick={() =>
                    this.setState({ isLoginView: !this.state.isLoginView })
                  }
                  className="btn btn-primary"
                  disabled={isSubmitting}
                >
                  {this.state.isLoginView ? "Регистрация" : "Вернуться"}
                </button>
              </div>
              {status && <div className={"alert alert-danger"}>{status}</div>}
            </Form>
          )}
        </Formik>
      </div>
    );
  }
}

export { LoginPage };
